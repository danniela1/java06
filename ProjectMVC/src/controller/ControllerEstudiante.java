package controller;

import java.util.ArrayList;
import java.util.function.Consumer;

import modelo.Estudiante;
import run.Run;

public class ControllerEstudiante extends Estudiante {
	//metodos agregar bscar
	
	
	
	public ControllerEstudiante() {
		// TODO Auto-generated constructor stub
	}

			


	public ControllerEstudiante(int id, String nombre, String apellidoPat, String apellidoMat, int edad, String carrera,
			int semestre, String domicilio, int telefono, String tipoSangre, String correo, String curp, String rfc,
			String atributo14, String atributo15, String atributo16, String atributo17, String atributo18,
			String atributo19, String atributo20) {
		super(id, nombre, apellidoPat, apellidoMat, edad, carrera, semestre, domicilio, telefono, tipoSangre, correo, curp, rfc,
				atributo14, atributo15, atributo16, atributo17, atributo18, atributo19, atributo20);
		// TODO Auto-generated constructor stub
	}


	/**
	 * <p>Metodo encargado de agregar estudiantes a la lista existente, llamada 'listaEstudiante'. </p>
	 * @param listaEstudiante 'listaEstudiante' lista que almacena los registros totales.
	 * @param prod registro a agregar a la lista.
	 * 
	 * 
	 * @author nnavarrete@externosdeloittemx.com
	 * @version 1.0
	 * @since 11/03/2020
	 */
	public static void addEst(ArrayList<ControllerEstudiante> listaEstudiante, ControllerEstudiante est) {
		listaEstudiante.add(est);
	}

	

	public void agregar(ControllerEstudiante est) {
		Run.listaEstudiante.add(est);
	}

	public static void agregar(int id, String nombre, String apellidoPat, String apellidoMat, int edad) {
		
		try {
			ControllerEstudiante.agregar(id, nombre, apellidoPat, apellidoMat, edad);
			
		}catch(Exception ex) {
			
		}
		
		  
	}

	public static void agregar2(String carrera, int semestre, String domicilio, int telefono, String tipoSangre) {
		
		ControllerEstudiante.agregar2(carrera, semestre, domicilio, telefono, tipoSangre);  
	}

	public static void agregar3(String correo, String curp, String rfc, String atributo14, String atributo15) {
		
		ControllerEstudiante.agregar3(correo, curp, rfc, atributo14, atributo15);  
	}

	public static void agregar4(String atributo16, String atributo17, String atributo18, String atributo19, String atributo20) {
	
	ControllerEstudiante.agregar4(atributo16, atributo17, atributo18, atributo19, atributo20);  
	}
	
	/**
	 * metodo que se encarga de buscar el estudiante dentro del arreglo, dependiendo del nombre ingresado.
	 * @param listaEstudiante de la cual se extraerá el nombre a buscar
	 * @param nombre : string que comparará con la lista existente
	 * @return cadena del estudiante encontrado
	 * 
	 * 
	 * @author nnavarrete@externosdeloittemx.com
	 * @version 1.0
	 * @since 11/03/2020
	 * 
	 */
	/*public static String buscarEstudiante(ArrayList<ControllerEstudiante> listaEstudiante, String nombre) {
		//String stringBuffer = "";
		
		for(ControllerEstudiante a: Run.listaEstudiante) {
            if(nombre.equals(a.getApellidoPat())) return a.toString();
            return "Encontrado" + a;
        }
        return "Persona no encontrada\n";

	}
	*/
	
	/**
	 * Metodo encargado de realizar una busqueda acorde al apellido del estudiante dentro de la lista
	 * @param paterno string
	 * @return
	 */
	public static String buscarPaterno(String paterno) {
        for(ControllerEstudiante a: Run.listaEstudiante) {
            if(paterno.equals(a.getApellidoPat())) return a.toString();
            return "Encontrado" + a;
            
        }
        return "Persona no encontrada";
    }

	
	


    

}
