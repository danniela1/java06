package modelo;

public abstract class Estudiante {

	private int id;
	private String nombre;
	private String apellidoPat;
	private String apellidoMat;
	private int edad;
	private String carrera;
	private int semestre;
	private String domicilio;
	private int telefono;
	private String tipoSangre;
	private String correo;
	private String curp;
	private String rfc;
	private String atributo14;
	private String atributo15;
	private String atributo16;
	private String atributo17;
	private String atributo18;
	private String atributo19;
	private String atributo20;
	
	public Estudiante() {
		// TODO Auto-generated constructor stub
	}
	
	

	public Estudiante(int id, String nombre, String apellidoPat, String apellidoMat, int edad, String carrera,
			int semestre, String domicilio, int telefono, String tipoSangre, String correo, String curp, String rfc,
			String atributo14, String atributo15, String atributo16, String atributo17, String atributo18,
			String atributo19, String atributo20) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellidoPat = apellidoPat;
		this.apellidoMat = apellidoMat;
		this.edad = edad;
		this.carrera = carrera;
		this.semestre = semestre;
		this.domicilio = domicilio;
		this.telefono = telefono;
		this.tipoSangre = tipoSangre;
		this.correo = correo;
		this.curp = curp;
		this.rfc = rfc;
		this.atributo14 = atributo14;
		this.atributo15 = atributo15;
		this.atributo16 = atributo16;
		this.atributo17 = atributo17;
		this.atributo18 = atributo18;
		this.atributo19 = atributo19;
		this.atributo20 = atributo20;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidoPat() {
		return apellidoPat;
	}

	public void setApellidoPat(String apellidoPat) {
		this.apellidoPat = apellidoPat;
	}

	public String getApellidoMat() {
		return apellidoMat;
	}

	public void setApellidoMat(String apellidoMat) {
		this.apellidoMat = apellidoMat;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCarrera() {
		return carrera;
	}

	public void setCarrera(String carrera) {
		this.carrera = carrera;
	}

	public int getSemestre() {
		return semestre;
	}

	public void setSemestre(int semestre) {
		this.semestre = semestre;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getTipoSangre() {
		return tipoSangre;
	}

	public void setTipoSangre(String tipoSangre) {
		this.tipoSangre = tipoSangre;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getCurp() {
		return curp;
	}

	public void setCurp(String curp) {
		this.curp = curp;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getAtributo14() {
		return atributo14;
	}

	public void setAtributo14(String atributo14) {
		this.atributo14 = atributo14;
	}

	public String getAtributo15() {
		return atributo15;
	}

	public void setAtributo15(String atributo15) {
		this.atributo15 = atributo15;
	}

	public String getAtributo16() {
		return atributo16;
	}

	public void setAtributo16(String atributo16) {
		this.atributo16 = atributo16;
	}

	public String getAtributo17() {
		return atributo17;
	}

	public void setAtributo17(String atributo17) {
		this.atributo17 = atributo17;
	}

	public String getAtributo18() {
		return atributo18;
	}

	public void setAtributo18(String atributo18) {
		this.atributo18 = atributo18;
	}

	public String getAtributo19() {
		return atributo19;
	}

	public void setAtributo19(String atributo19) {
		this.atributo19 = atributo19;
	}

	public String getAtributo20() {
		return atributo20;
	}

	public void setAtributo20(String atributo20) {
		this.atributo20 = atributo20;
	}
	
	
}
